<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenistas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $correo
 * @property int $activo
 * @property string $fechaBaja
 * @property string $fechaNacimiento
 * @property int $altura
 * @property int $peso
 * @property int $idNaciones
 *
 * @property Naciones $naciones
 */
class Tenistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo', 'altura', 'peso', 'idNaciones'], 'integer'],
            [['fechaBaja', 'fechaNacimiento'], 'safe'],
            [['nombre'], 'string', 'max' => 30],
			[['correo'],'email'],
            [['idNaciones'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['idNaciones' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'activo' => 'Activo',
            'fechaBaja' => 'Fecha de Baja',
            'fechaNacimiento' => 'Fecha de Nacimiento',
            'altura' => 'Altura(cm)',
            'peso' => 'Peso(kg)',
            'idNaciones' => 'Id Naciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaciones()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'idNaciones']);
    }
}
